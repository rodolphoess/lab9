#Makefile for "Laboratório 9" C++ application
#Created by Rodolpho Erick 20/06/2017

# Comandos do sistema operacional
# Linux: rm -rf
# Windows: cmd //C del
RM = rm -rf

# Compilador
CC = g++

# Variaveis para os subdiretorios
BIN_DIR = ./bin
DAT_DIR = ./data
INC_DIR = ./include
LIB_DIR = ./lib
SRC_DIR = ./src
OBJ_DIR = ./build

DOC_DIR = ./doc
TEST_DIR = ./test

# Outras variaveis

# Opcoes de compilacao
CFLAGS01 = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

#SO Linux
linux: rodolpho.a rodolpho.so prog_estatico prog_dinamico

#SO Windows
windows: rodolpho.lib rodolpho.dll prog_estatico.exe prog_dinamico.exe

# Garante que os alvos desta lista nao sejam confundidos com arquivos de mesmo nome
.PHONY: all clean doxy debug

# Define o alvo (target) para a compilacao completa.
# Ao final da compilacao, remove os arquivos objeto.
all: clean rodolpho

debug: CFLAGS01 += -g -O0
debug: clean rodolpho

# Alvo (target) para a criação da estrutura de diretorios
# necessaria para a geracao dos arquivos objeto
init:
	@mkdir -p $(OBJ_DIR)
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(LIB_DIR)

#======================================================

#LINUX

# Alvo (target) para a construcao da biblioteca estática rodolpho.a
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.a: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/rodolpho.h 
	$(CC) $(CFLAGS) -c $(SRC_DIR)/rodolpho.cpp -o $(OBJ_DIR)/rodolpho.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o	
	@echo "+++ [Biblioteca estática criada em $(LIB_DIR)/$@] +++"
	@echo "============="

# Alvo (target) para a construcao da biblioteca dinâmica rodolpho.a
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.so: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/rodolpho.h 
	$(CC) $(CFLAGS) -fPIC -c $(SRC_DIR)/rodolpho.cpp -o $(OBJ_DIR)/rodolpho.o
	$(CC) -shared -fPIC -o $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o
	@echo "+++ [Biblioteca dinâmica criada em $(LIB_DIR)/$@] +++"
	@echo "============="

prog_estatico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.a -o $(OBJ_DIR)/$@

prog_dinamico:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.so -o $(OBJ_DIR)/$@

#======================================================

#WINDOWS

# Alvo (target) para a construcao da biblioteca estática rodolpho.lib
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.lib: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/rodolpho.h 
	$(CC) $(CFLAGS) -c $(SRC_DIR)/rodolpho.cpp -o $(OBJ_DIR)/rodolpho.o
	$(AR) rcs $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o	
	@echo "+++ [Biblioteca estática criada em $(LIB_DIR)/$@] +++"
	@echo "============="

# Alvo (target) para a construcao da biblioteca dinâmica rodolpho.a
# Define os arquivos rodolpho.cpp e rodolpho.h como dependências
rodolpho.dll: $(SRC_DIR)/rodolpho.cpp $(INC_DIR)/rodolpho.h 
	$(CC) $(CFLAGS) -c $(SRC_DIR)/rodolpho.cpp -o $(OBJ_DIR)/rodolpho.o
	$(CC) -shared -o $(LIB_DIR)/$@ $(OBJ_DIR)/rodolpho.o
	@echo "+++ [Biblioteca dinâmica criada em $(LIB_DIR)/$@] +++"
	@echo "============="

prog_estatico.exe:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.lib -o $(OBJ_DIR)/$@

prog_dinamico.exe:
	$(CC) $(CFLAGS) $(SRC_DIR)/main.cpp $(LIB_DIR)/rodolpho.dll -o $(OBJ_DIR)/$@

#======================================================

# Alvo (target) para a geração automatica de documentacao usando o Doxygen.
# Sempre remove a documentacao anterior (caso exista) e gera uma nova.
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile

# Alvo (target) usado para limpar os arquivos temporarios (objeto)
# gerados durante a compilacao, assim como os arquivos binarios/executaveis.
clean:	
	$(RM) $(OBJ_DIR)/*