/**
* @file 	rodolpho.h
* @brief    Arquivo cabeçalho com as definições das funções que realizam busca, ordenação
*			e conteiners.
* @author   Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    20/06/2017
* @date 	20/06/2017
*/

#ifndef  RODOLPHO_H
#define  RODOLPHO_H

#include <iostream>

using namespace std;

namespace edb1 {

	/**
	* @brief        	Função que realiza busca binária em um vetor ordenado iterativamente
	* @param num        Inteiro com a chave de busca
	* @param left       Inteiro com o início do vetor
	* @param right      Inteiro com o fim do vetor
	* @param *v         Ponteiro apontando para o vetor
	* @return       	Retorna true caso a chave esteja, ou false caso não
	*/
	int binarySearchI(int num, int left, int right, int *v, int passos);

	/**
	* @brief        	Função que realiza busca binária em um vetor ordenado recursivamente
	* @param num        Inteiro com a chave de busca
	* @param left       Inteiro com o início do vetor
	* @param right      Inteiro com o fim do vetor
	* @param *v         Ponteiro apontando para o vetor
	* @return       	Retorna true caso a chave esteja, ou false caso não
	*/
	int binarySearchR(int num, int left, int right, int *v, int passos);

	/**
	* @brief        	Função que realiza busca sequencial em um vetor ordenado iterativamente
	* @param x        	Inteiro com a chave de busca
	* @param n       	Inteiro com o tamanho do vetor
	* @param *v         Ponteiro apontando para o vetor
	* @return       	Retorna true caso a chave esteja, ou false caso não
	*/
	int sequencialSearchI(int *v, int n, int x, int passos);

	/**
	* @brief        	Função que realiza busca sequencial em um vetor ordenado recursivamente
	* @param x        	Inteiro com a chave de busca
	* @param n       	Inteiro com o tamanho do vetor
	* @param *v         Ponteiro apontando para o vetor
	* @return       	Retorna true caso a chave esteja, ou false caso não
	*/
	int sequencialSearchR(int *v, int n, int x, int passos);

	/**
	* @brief			Função que realiza a ordenação por seleção em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void selectionSort(int *v, int tamanho);

	/**
	* @brief			Função que realiza a ordenação por inserção em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void insertionSort(int *v, int tamanho);

	/**
	* @brief			Função que realiza a ordenação por bolha em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void bubbleSort(int *v, int tamanho);

	/**
	* @brief			Função que realiza a ordenação quicksort em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void quickSort(int *v, int tamanho);

	/**
	* @brief			Função que realiza a ordenação mergesort em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param low		Primeiro elemento do vetor
	* @param high		Ultimo elemento do vetor
	*/
	void mergeSort(int *v, int low, int high);

	/**
	* @brief			Função que realiza a intercalacao durante a ordenacao mergesort em um vetor
	* @param *a			Vetor intercalado com os elementos desordenados
	* @param low		Primeiro elemento do vetor
	* @param high		Ultimo elemento do vetor
	* @param mid		Elemento do meio no vetor
	*/
	void intercalar(int *a, int low, int high, int mid);

}

#endif
