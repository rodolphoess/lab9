var menudata={children:[
{text:"Página principal",url:"index.html"},
{text:"Namespaces",url:"namespaces.html",children:[
{text:"Lista de namespaces",url:"namespaces.html"},
{text:"Membros do namespace",url:"namespacemembers.html",children:[
{text:"Tudo",url:"namespacemembers.html"},
{text:"Funções",url:"namespacemembers_func.html"}]}]},
{text:"Ficheiros",url:"files.html",children:[
{text:"Lista de ficheiros",url:"files.html"},
{text:"Membros dos Ficheiros",url:"globals.html",children:[
{text:"Tudo",url:"globals.html"},
{text:"Funções",url:"globals_func.html"}]}]}]}
