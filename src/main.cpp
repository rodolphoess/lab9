/**
* @file 	main.cpp
* @brief 	Funcao principal do programa
* @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
* @since 	20/06/2017
* @date 	20/06/2017
* @sa		rodolpho.h
*/

#include <iostream>

#include "rodolpho.h"

using namespace std;
using namespace edb1;

/**
 * @brief Função principal.
 */
int main() {
	int tamanho = 10, passos = 0;
	int vSort[tamanho] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int vUnsort[tamanho] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};

	int numPassos1 = binarySearchR(2, 0, sizeof(vSort) / 4, vSort, passos);
	int numPassos2 = binarySearchI(2, 0, sizeof(vSort) / 4, vSot, passos);
	int numPassos3 = sequencialSearchI(vSort, sizeof(vSort) / 4, 2, passos);
	int numPassos4 = sequencialSearchR(vSort, sizeof(vSort) / 4, 2, passos);

	selectionSort(vUnsort, 10);
	insertionSort(vUnsort, 10);
	bubbleSort(vUnsort, 10);
	quickSort(vUnsort, 10);
	mergeSort(vUnsort, 10, 1);


	return 0;
}