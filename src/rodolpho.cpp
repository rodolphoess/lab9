/**
* @file 	rodolpho.cpp
* @brief	Arquivo de corpo com as implementações das funções que realizam busca, ordenação
*			e conteiners.
* @author	Rodolpho Erick (rodolphoerick90@gmail.com)
* @since    20/06/2017
* @date 	20/06/2017
* @sa 		rodolpho.h
*/

#include "rodolpho.h"

namespace edb1 {

	/**
	* @brief			Função que realiza busca binária em um vetor ordenado iterativamente
	* @param num		Inteiro com a chave de busca
	* @param left		Inteiro com o início do vetor
	* @param right		Inteiro com o fim do vetor
	* @param *v 		Ponteiro apontando para o vetor
	* @return 			Retorna true caso a chave esteja, ou false caso não
	*/
	int binarySearchI(int num, int left, int right, int *v, int passos) {
		int mid;	
		while( left <= right ) { /**< vetor vazio, não encontrou o elemento */
			mid = ( ( left + right ) / 2 ) ; /**< posição do meio */
			if ( v[mid] == num ){ /**< encontrou o elemento */
				passos++;
				return passos;
			} else if( v[mid] < num ) {/**< busca na segunda metade */
				left = mid + 1 ;
				passos++;
			} else if( v[mid] > num ){/**< busca na primeira metade */
				right = mid - 1 ;
				passos++;
			}
		}
		return passos;
	}

	/**
	* @brief			Função que realiza busca binária em um vetor ordenado recursivamente
	* @param num		Inteiro com a chave de busca
	* @param left		Inteiro com o início do vetor
	* @param right		Inteiro com o fim do vetor
	* @param *v 		Ponteiro apontando para o vetor
	* @return 			Retorna true caso a chave esteja, ou false caso não
	*/
	int binarySearchR(int num, int left, int right, int *v, int passos){	
		if (left > right) /**< se vazio, não encontrou o elemento */
			return passos;

		int mid = ( (left + right ) / 2 ); /**< Indice do elementro central */

		if(v[mid] == num){
			passos++;
			return passos; /**< Encontrou o elemento */
		}

		if(v[mid] < num) {
			passos++;
			return binarySearchR(num, mid + 1, right, v, passos); /**< busca na segunda metade */
		} else if(v[mid] > num) {
			passos++;
			return binarySearchR(num, left, mid - 1, v, passos); /**< busca primeira metade */
		}
		return passos;
	}

	/**
	* @brief        	Função que realiza busca sequencial em um vetor ordenado iterativamente
	* @param x        	Inteiro com a chave de busca
	* @param n       	Inteiro com o tamanho do vetor
	* @param *v         Ponteiro apontando para o vetor
	* @return       	Retorna true caso a chave esteja, ou false caso não
	*/
	int sequencialSearchI(int *v, int n, int x, int passos){	
		for(int i = 0; i < n; i++){
			if(v[i] > x)
				
				break;
			if(v[i] == x ){
				passos++;
				return passos;
			}
			passos++;
		}
		return passos;
	}

	/**
	* @brief        	Função que realiza busca sequencial em um vetor ordenado recursivamente
	* @param x        	Inteiro com a chave de busca
	* @param n       	Inteiro com o tamanho do vetor
	* @param *v         Ponteiro apontando para o vetor
	* @return       	Retorna true caso a chave esteja, ou false caso não
	*/
	int sequencialSearchR(int *v, int n, int x, int passos) {	
		if(n <= 0)
			return passos;
		if(v[n-1] == x){
			passos++;
			return passos;
		} else {
			passos++;
			return sequencialSearchR(v, n - 1, x, passos);
		}	

		return passos;
	}

	/**
	* @brief			Função que realiza a ordenação por seleção em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void selectionSort(int *v, int tamanho) {
		for (int ii = 0; ii < tamanho - 1; ii++) {
			int menor = ii;
				for (int jj = ii + 1; jj < tamanho; jj++) {
					if (v[jj] < v[menor]) menor = jj;
				}
			if (menor != ii) {
				float aux = v[ii];
				v[ii] = v[menor];
				v[menor] = aux;
			}
		}
	}

	/**
	* @brief			Função que realiza a ordenação por inserção em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void insertionSort(int *v, int tamanho) {
		for (int ii = 0; ii < tamanho; ii++) {
			//int el = v[ii];
				for (int jj = ii; jj >= 1 && v[jj] < v[jj - 1]; jj--) {
					float aux = v[jj];
					v[jj] = v[jj - 1];
					v[jj - 1] = aux;
				}
		}
	}

	/**
	* @brief			Função que realiza a ordenação por bolha em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void bubbleSort(int *v, int tamanho) {
		for (int ii = 0; ii < tamanho; ii++) {
			for (int jj = 0; jj < tamanho - 1; jj++) {
				if (v[jj] > v[jj + 1]) {
					float aux = v[jj];
					v[jj] = v[jj + 1];
					v[jj + 1] = aux;
				}
			}
		}
	}

	/**
	* @brief			Função que realiza a ordenação quicksort em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param tamanho	Inteiro com o tamanho do vetor
	*/
	void quickSort(int *v, int tamanho) {
		if (tamanho <= 1) return;

		int esq = 1;
		int dir = tamanho - 1;

		while (esq >= dir) {
			while ((v[esq <= v[0]]) && (esq < dir)) {
				esq++;
			}

			while (v[dir] > v[0]) {
				dir--;
			}

			if (v[dir] > v[esq]) {
				int aux = v[esq];
				v[esq] = v[dir];
				v[dir] = aux;
			}
		}

	}

	/**
	* @brief			Função que realiza a ordenação mergesort em um vetor
	* @param *v			Vetor com os elementos desordenados
	* @param low		Primeiro elemento do vetor
	* @param high		Ultimo elemento do vetor
	*/
	void mergeSort(int *v, int low, int high) {
		int mid;

		if (low < high) {
			mid = (low + high) / 2;
			mergeSort(v, low, mid);
			mergeSort(v, mid + 1, high);
			intercalar(v, low, high, mid);
		}
	}

	/**
	* @brief			Função que realiza a intercalacao durante a ordenacao mergesort em um vetor
	* @param *a			Vetor intercalado com os elementos desordenados
	* @param low		Primeiro elemento do vetor
	* @param high		Ultimo elemento do vetor
	* @param mid		Elemento do meio no vetor
	*/
	void intercalar(int *a, int low, int high, int mid) {
		int i = low, j = mid + 1, k = low, c[50];

		while (i <= mid && j <= high) {
			if (a[i] < a[j]) {
				c[k] = a[i];
				k++;
				i++;
			} else {
				c[k] = a[j];
				k++;
				j++;
			}
		}

		while (i <= mid) {
			c[k] = a[i];
			k++;
			i++;
		}

		while (j <= high) {
			c[k] = a[j];
			k++;
			j++;
		}

		for (i = low; i < k; i++) {
			a[i] = c[i];
		}
	}

}

